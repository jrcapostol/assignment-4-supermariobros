package jonapostol.supermariobros;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;


public class MainActivity extends ActionBarActivity {
    SMBView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        view = new SMBView(getBaseContext());
        super.onCreate(savedInstanceState);
        setContentView(view);
    }

    @Override
    protected void onPause(){
        super.onPause();
        //view.level.mariomusic.pause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        //view.level.mariomusic.start();
    }

}
