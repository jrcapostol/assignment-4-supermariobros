package jonapostol.supermariobros;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Jon Apostol on 5/6/2015.
 */
public class SMBView extends SurfaceView implements SurfaceHolder.Callback {
    Thread renderThread;
    public int WIDTH;
    public int HEIGHT;

    //SPLASH SCREEN
    private Bitmap splash;
    private Paint splashPaint = new Paint();
    private Paint splashText = new Paint();
    private Rect splashRec;
    protected boolean exitSplash = false;

    //D-PAD
    private Bitmap bitmapDPad;
    private Paint dpadPaint = new Paint();
    private Paint touchPoint;
    private Rect dpadRect;
    private boolean left;
    private boolean right;

    //BUTTON
    private Bitmap bitmapButton;
    private Paint buttonPaint;
    private Rect buttonRect;
    private int buttonAlpha = 60;

    //FIRE BUTTON
    private Bitmap fireButton;
    private Paint firePaint;
    private Rect fireRect;
    private int fireAlpha = 60;

    protected boolean scrollLevel = false;

    //SCORE
    protected Score score;

    //LEVEL
    protected Level level;




    public SMBView(Context context){
        super(context);
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated ( SurfaceHolder holder ) {
        WIDTH = getWidth();
        HEIGHT = getHeight();
        renderThread = new SMBThread(this);
        renderThread.start();

        //SPLASH SCREEN
        splash = BitmapFactory.decodeResource(this.getResources(), R.drawable.splash);
        splashPaint = new Paint();
        splashPaint.setAlpha(255);
        splashRec = new Rect(0, 0, getWidth(), getHeight());
        splashText.setTextSize(getHeight() / 20);
        splashText.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        splashText.setColor(Color.WHITE);

    // CREATES SCORE1 AND LEVEL FIELDS
        score = new Score(this);
        level = new LevelOne(this);


    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder){

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){

    }

    @Override
    public boolean onTouchEvent ( MotionEvent e ) {

        int curX = (int)e.getX();
        int curY = (int)e.getY();

        switch ( e.getAction() ) {
            case MotionEvent.ACTION_MOVE:
                //LEFT BUTTON PUSHED
                if(curX >= 0 && curX <= getWidth()/8 && curY >= 5*getHeight()/9 && curY < 8*getHeight()/9){
                    if(!level.mario.blockedR) {
                        right = false;
                        left = true;
                        level.mario.isMoving(false);
                    }
                }
                //RIGHT BUTTON PUSHED
                if(curX >= 2*getWidth()/8 && curX <= 3*getWidth()/8 && curY >= 5*getHeight()/9 && curY < 8*getHeight()/9 && !level.mario.blockedR){
                    left = false;
                    right = true;
                    if(!level.mario.blockedR) {
                        if (level.mario.x < getWidth() / 2 || level.stopScrolling)
                            level.mario.isMoving(true);
                        else
                            level.scrollLevel();
                    }
                }
                break;

            case MotionEvent.ACTION_DOWN:
                if(!exitSplash) {
                    exitSplash = true;
                    level.mariomusic.start();
                }
                // MOVES MARIO EITHER LEFT, RIGHT OR JUMP.

                //JUMP BUTTON PUSHED
                if(curX >= 5*getWidth()/8 && curX <= getWidth() && curY >= 5*getHeight()/9 && curY < 8*getHeight()/9){
                    buttonAlpha = 200;
                    level.mario.isJumping();
                }
                //FIRE BUTTON PRESSED
                if(curX >= getWidth()-getWidth()/16 && curX <= getWidth() && curY >= 0 && curY < getHeight()/9 ){
                    fireAlpha = 200;
                    level.shootFire = true;
                }

                //LEFT BUTTON PUSHED
                if(curX >= 0 && curX <= getWidth()/8 && curY >= 5*getHeight()/9 && curY < 8*getHeight()/9 && !level.mario.blockedL){
                    right = false;
                    left = true;
                    level.mario.isMoving(false);
                }
                //RIGHT BUTTON PUSHED
                if(curX >= 2*getWidth()/8 && curX <= 3*getWidth()/8 && curY >= 5*getHeight()/9 && curY < 8*getHeight()/9 && !level.mario.blockedR){
                    left = false;
                    right = true;
                    if(level.mario.x < getWidth()/2)
                        level.mario.isMoving(true);
                    else
                        level.scrollLevel();
                }
                break;

            case MotionEvent.ACTION_UP:
                // DOES NOTHING.
                left = false;
                right = false;
                buttonAlpha = 60;
                fireAlpha = 60;
                level.shootFire = false;
                break;
        }
        return true ;
    }

    public void onDraw(Canvas c){
        super.onDraw(c);

        if(exitSplash) {
            //LEVEL
            level.tick(c);

            // CREATES D-PAD
            bitmapDPad = BitmapFactory.decodeResource(this.getResources(), R.drawable.dpad);
            dpadPaint = new Paint();
            dpadPaint.setAlpha(50);
            dpadRect = new Rect(0, 3 * getHeight() / 9, 3 * getWidth() / 8, getHeight());
            c.drawBitmap(bitmapDPad, null, dpadRect, dpadPaint);

            // CREATES BUTTON
            bitmapButton = BitmapFactory.decodeResource(this.getResources(), R.drawable.button);
            buttonPaint = new Paint();
            buttonPaint.setAlpha(buttonAlpha);
            buttonRect = new Rect(5 * getWidth() / 8, 3 * getHeight() / 9, getWidth(), getHeight());
            c.drawBitmap(bitmapButton, null, buttonRect, buttonPaint);

            //FIRE BUTTON
            fireButton = BitmapFactory.decodeResource(this.getResources(), R.drawable.firebutton);
            firePaint = new Paint();
            firePaint.setAlpha(fireAlpha);
            fireRect = new Rect(getWidth()-getWidth()/16, 0, getWidth(), getHeight()/9);
            if(level.mario.fireOn)
                c.drawBitmap(fireButton, null, fireRect, firePaint);

            //CREATES FIRE BUTTON


            // DISPLAYS SCORE
            score.tick(c);


            //SHOWS WHAT BUTTON WAS PRESSED
            if (left) {
                touchPoint = new Paint();
                touchPoint.setColor(Color.WHITE);
                touchPoint.setAlpha(190);
                c.drawCircle(getWidth() / 16, 2 * getHeight() / 3, getWidth() / 16, touchPoint);
            }
            if (right) {
                touchPoint = new Paint();
                touchPoint.setColor(Color.WHITE);
                touchPoint.setAlpha(190);
                c.drawCircle(5 * getWidth() / 16, 2 * getHeight() / 3, getWidth() / 16, touchPoint);
            }

        }
        else{
            c.drawBitmap(splash, null, splashRec, splashPaint);
            c.drawText("Press Screen to Start", 3*getWidth()/8, 5*getHeight()/9, splashText);
        }
    }

    public void tick(Canvas c){

        onDraw(c);
    }
}
