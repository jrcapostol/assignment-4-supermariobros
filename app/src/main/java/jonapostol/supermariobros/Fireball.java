package jonapostol.supermariobros;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Jon Apostol on 5/20/2015.
 */
public class Fireball {
    protected int x;
    protected int y;

    private Bitmap fireballImage;
    private Rect fbRec;
    private SMBView view;
    private Paint paint;

    public Fireball(int x, int y, SMBView view){
        this.x = x;
        this.y = y;
        this.view = view;

        paint = new Paint();
        paint.setAlpha(255);

        fbRec = new Rect(x, y, x + view.getWidth()/16, view.getHeight()/9);
        fireballImage = BitmapFactory.decodeResource(view.getResources(), R.drawable.fireball);
    }

    public void physics(){
        x += view.getWidth()/144;
    }

    public void tick(Canvas c){
        c.drawBitmap(fireballImage, null, fbRec, paint);
    }
}
