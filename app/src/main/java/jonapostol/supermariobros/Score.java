package jonapostol.supermariobros;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

/**
 * Created by Jon Apostol on 5/12/2015.
 */
public class Score {
    private SMBView view;
    private Paint paint;

    protected int life = 3;
    private int score = 0;
    private int time = 999;
    private int cnt = 0;

    protected boolean gameOver = false;
    protected boolean finished = false;

    public Score(SMBView view){
        this.view = view;
        paint = new Paint();
        paint.setTextSize(view.getHeight()/20);
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        paint.setColor(Color.WHITE);
    }

    public void addScore(int add){
        score += add;
    }

    public void addRemainingTimeToScore(){ score += time; }

    public void resetScore(){
        score = 0;
        life = 3;
    }

    public void resetTime(){
        time = 999;
    }

    public void addLife(){
        life++;
    }

    public void loseLife(){
        life--;
    }

    public void tick(Canvas c){
        if(!finished) {
            cnt++;
            if (cnt % 10 == 0 && cnt != 0)
                time--;
            if(time <= 0 || life < 0){
            /*
            TELLS VIEW THAT GAME IS OVER
             */
                gameOver = true;
                cnt = 0;
            }
        }
        else{
            score += time;
            time = 0;
            if(score >= 1000)
                life++;
        }
        //TIME
        c.drawText("TIME: " + time, view.getWidth()/9, view.getHeight()/9, paint);
        //LIFE
        c.drawText("LIFE: " + life, view.getWidth()/2, view.getHeight()/9, paint);
        //SCORE
        c.drawText("SCORE: " + score, 7*view.getWidth()/9, view.getHeight()/9, paint);
    }
}
