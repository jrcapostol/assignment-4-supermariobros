package jonapostol.supermariobros;

import android.graphics.Canvas;
import android.media.MediaPlayer;

/**
 * Created by Jon Apostol on 5/15/2015.
 */
public abstract class Level {
    protected SMBView view;
    protected Fireball fireball;
    protected boolean shootFire;
    protected Mario mario;
    protected boolean finished;
    protected int width;
    protected int height;
    protected Material[][] init;
    protected Enemy[] enemies;
    protected boolean stopScrolling;
    protected MediaPlayer mariomusic;

    public Level(SMBView view){
        this.view = view;
    }

    public abstract void tick(Canvas c);

    public abstract Level switchTo();

    protected abstract void scrollLevel();
}
