package jonapostol.supermariobros;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.MediaPlayer;

/**
 * Created by Jon Apostol on 5/6/2015.
 */
public class Mario {
    //POSITION
    protected int x;
    protected int y;
    private Bitmap pic;
    private Paint paint;
    private Rect rect;
    private int alpha = 255;

    //PARTS OF MARIO
    private int bottom;

    //VIEW'S CHARACTERISTICS
    private SMBView view;
    private int height;
    private int width;

    //STATUS
    private int originalY;
    protected boolean fireOn;
    protected int growth;
    protected boolean grown = false;
    private boolean isJumping = false;
    private boolean isFalling = false;
    protected boolean dead = false;
    private boolean hitBrick = false;
    private boolean onBrick = false;
    protected boolean blockedR = false;
    protected boolean blockedL = false;


    public Mario(int x, int y, SMBView view){
        growth = 0;
        bottom = y + view.getHeight()/9;
        this.view = view;
        this.height = view.HEIGHT;
        this.width = view.WIDTH;

        this.x = x;
        this.y = y;

        paint = new Paint();
        paint.setAlpha(alpha);
        pic = BitmapFactory.decodeResource(view.getResources(), R.drawable.mario);
    }

    public void isMoving(boolean direction){
        //GOES RIGHT
        if(x < width/2 && direction)
            x += width/144;
        else{
             if (x > 0)
             x -= width / 144;
        }
    }

    public void tick(Canvas c){
        physics();
        //FINAL DRAW
        rect = new Rect(x, y - growth, x + view.getWidth()/16, y + view.getHeight()/9);
        c.drawBitmap(pic, null, rect, paint);
    }

    private void physics() {
        if(y >= view.getHeight()){
            isFalling = false;
            isJumping = false;
            y = view.getWidth()/2;
        }
        if(onBrick) {
            isFalling = false;
            pic = BitmapFactory.decodeResource(view.getResources(), R.drawable.mario);
        }
        if(hitBrick) {
            isFalling = true;
            pic = BitmapFactory.decodeResource(view.getResources(), R.drawable.jumpingmario);
        }
        if(!onBrick && !isJumping) {
            isFalling = true;
            pic = BitmapFactory.decodeResource(view.getResources(), R.drawable.jumpingmario);
        }
        if(isFalling) {
            isJumping = false;
            y += view.getHeight() / 9;
            pic = BitmapFactory.decodeResource(view.getResources(), R.drawable.jumpingmario);
        }
        else if(isJumping){
            pic = BitmapFactory.decodeResource(view.getResources(), R.drawable.jumpingmario);
            if( (y >= (originalY - 3 * view.getHeight()/9)) || !hitBrick)
                y -= view.getHeight()/9;
            else{
                isFalling = true;
                isJumping = false;
            }
            if(y <= 2*view.getHeight()/9){
                isFalling = true;
                isJumping = false;
            }

        }
    }

    public void isJumping(){
        if(!isFalling && !isJumping) {
            pic = BitmapFactory.decodeResource(view.getResources(), R.drawable.jumpingmario);
            MediaPlayer jumpSXF = MediaPlayer.create(view.getContext(), R.raw.smalljump);
            jumpSXF.setLooping(false);
            jumpSXF.start();
            isJumping = true;
        }
    }

    private void blockedPath(Material mat){
        if(x + view.getWidth()/16 + view.getWidth()/32 >= mat.x && y == mat.y)
            blockedR = true;
        else if(x + view.getWidth()/32 <= mat.x + view.getWidth()/16 && y == mat.y)
            blockedL = true;
        else{
            blockedR = false;
            blockedL = false;
        }
    }

    protected boolean touched(Material mat){
        if(x + view.getWidth()/16 - view.getWidth()/32 >= mat.x && y == mat.y)
            blockedR = true;
        else if(x + view.getWidth()/32 <= mat.x + view.getWidth()/16 && y == mat.y)
            blockedL = true;
        else{
            blockedR = false;
            blockedL = false;
        }
        if (x + view.getWidth()/16 - view.getWidth()/32 >= mat.x  && x + view.getWidth()/32 <= mat.x + view.getWidth()/16){
            switch(mat.type){
                //EMPTY
                case 0:
                    hitBrick = false;
                    onBrick = false;
                    break;
                //FLOOR
                case 1:
                    //ON TOP
                    if(y + view.getHeight()/9 >= mat.y && mat.exists) {
                        onBrick = true;
                        isFalling = false;
                        originalY = mat.y;
                    }
                    //BELOW
                    if((y - growth == mat.y + view.getHeight()/9) && (y - growth + view.getHeight()/18 >= mat.y) && mat.exists) {
                        isJumping = false;
                        isFalling = true;
                        hitBrick = true;
                    }
                    return true;
                //BRICK
                case 2:
                    //ON TOP
                    if(y + view.getHeight()/9 == mat.y && mat.exists) {
                        onBrick = true;
                        isFalling = false;
                        originalY = mat.y;
                    }
                    else if(y + view.getHeight()/9 == mat.y && mat.exists && isFalling) {
                        onBrick = true;
                        isFalling = false;
                        originalY = mat.y;
                    }
                    //BELOW
                    if((y - growth <= mat.y + view.getHeight()/9) && (y - growth + view.getHeight()/18 >= mat.y) && mat.exists) {
                        isJumping = false;
                        isFalling = true;
                        mat.type = 0;
                        hitBrick = true;
                    }
                    return true;
                //COIN
                case 3:
                    if(y >= mat.y && y - growth <= mat.y + view.getHeight()/9){
                        MediaPlayer collectCoin = MediaPlayer.create(view.getContext(), R.raw.coin);
                        collectCoin.setLooping(false);
                        collectCoin.start();
                        view.score.addScore(10);
                        mat.type = 0;
                        return true;
                    }
                //LEVEL UP
                case 7:
                    if(y >= mat.y && y - growth <= mat.y + view.getHeight()/9) {
                        MediaPlayer collectCoin = MediaPlayer.create(view.getContext(), R.raw.coin);
                        collectCoin.setLooping(false);
                        collectCoin.start();
                        view.score.addLife();
                        mat.type = 0;
                        return true;
                    }
                //GROW MUSHROOM
                case 8:
                    if(y >= mat.y && y <= mat.y + view.getHeight()/9) {
                        growth = view.getHeight()/9;
                        grown = true;
                        mat.type = 0;
                        return true;
                    }
                //FIRE FLOWER
                case 9:
                    if(y >= mat.y && y <= mat.y + view.getHeight()/9) {
                        if(!grown) {
                            growth = view.getHeight() / 9;
                            grown = true;
                        }
                        fireOn = true;
                        mat.type = 0;
                        return true;
                    }

                default:
                    break;

            }
        }
        return false;
    }
}
