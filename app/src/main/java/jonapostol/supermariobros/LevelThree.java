package jonapostol.supermariobros;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;

/**
 * Created by Kendall on 5/20/2015.
 */
public class LevelThree extends Level{

    public LevelThree(SMBView view) {
        super(view);

        //MUSIC
        mariomusic = MediaPlayer.create(view.getContext(), R.raw.overworld);
        mariomusic.setLooping(true);
        if (view.exitSplash)
            mariomusic.start();

        stopScrolling = false;

        mario = new Mario(view.getWidth() / 12, 6 * view.getHeight() / 9, view);

        enemies = new Enemy[4];

        enemies[0] = new Enemy(0, view.getWidth() / 2, 6 * view.getHeight() / 9, view);
        enemies[1] = new Enemy(1, 3 * view.getWidth() / 2, 6 * view.getHeight() / 9, view);
        enemies[2] = new Enemy(1, 3 * view.getWidth() / 2, 5 * view.getHeight() / 9, view);
        enemies[3] = new Enemy(0, 3*view.getWidth()/4, 6 * view.getHeight() / 9, view);

        super.width = 9;
        super.height = 50;

        init = new Material[width][height];
        //EMPTY AT TOP FOUR ROWS
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 50; j++) {
                //FOR BRICK
                if (i == 2 && (j == 20 || (j > 23 && j < 27))) {
                    init[i][j] = new Material(2, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                if(i == 4 && j == 3)
                    init[i][j] = new Material(9, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                //CASTLE CONSTRUCTION
                else if (i == 5 && j == 40) {
                    init[i][j] = new Material(4, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                else if (i == 6 && (j == 39 || j == 41)) {
                    init[i][j] = new Material(5, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                else if (i == 6 && j == 40) {
                    init[i][j] = new Material(6, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                //COIN
                else if (i == 6 && (j > 10 && j < 20)) {
                    init[i][j] = new Material(3, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                else
                    init[i][j] = new Material(0, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
            }
        }

        //FLOOR AT BOTTOM FOUR ROWS
        for (int i = 7; i < 9; i++) {
            for (int j = 0; j < 50; j++)
                init[i][j] = new Material(1, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
        }

    }

    protected void scrollLevel() {
        if (!stopScrolling) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 50; j++) {
                    init[i][j].x -= view.getWidth() / 144;
                }
            }
        }
    }

    public void tick(Canvas c) {
        //BACKGROUND
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.CYAN);
        paint.setAntiAlias(true);
        c.drawPaint(paint);

        //SPRITES
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 50; j++) {
                checkpoint(i);
                mario.touched(init[i][j]);
                init[i][j].draw(c);
            }
        }



        //MARIO
        mario.tick(c);

        //ENEMY
        //FIRST ENEMY
        enemies[0].move();
        enemies[3].move();
        //enemies[0].touched(mario);
        enemies[0].tick(c);
        enemies[3].tick(c);

        //SECOND ENEMY
        if(arrivedActivateEnemy()) {
            enemies[1].move();
            enemies[2].move();
            enemies[1].tick(c);
            enemies[2].tick(c);
        }


        if(mario.dead) {
            view.score.resetTime();
            view.score.loseLife();
            if(view.score.life < 0) {
                view.score.resetScore();
                view.level = new LevelOne(view);
            }
            else {
                view.level = new LevelThree(view);
            }
        }

        //END OF GAME
        if (arrivedCheckpoint())
            view.level = switchTo();

    }

    private void checkpoint(int i) {
        if (!stopScrolling) {
            if (mario.x >= init[i][40].x)
                stopScrolling = true;
        }
    }

    private boolean arrivedCheckpoint() {
            return (mario.x >= init[6][40].x);
        }

    private boolean arrivedActivateEnemy() {
            return (mario.x >= init[6][16].x);
        }

    public Level switchTo() {
        mariomusic.stop();
        view.score.addRemainingTimeToScore();
        view.score.resetTime();
        return new LevelOne(view);
    }
}
