package jonapostol.supermariobros;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;

/**
 * Created by Jon Apostol on 5/12/2015.
 */
public class LevelOne extends Level{

    public LevelOne(SMBView view){
        super(view);

        //MUSIC
        mariomusic = MediaPlayer.create(view.getContext(), R.raw.overworld);
        mariomusic.setLooping(true);
        if(view.exitSplash)
            mariomusic.start();

        stopScrolling = false;

        mario = new Mario(view.getWidth()/12, 6*view.getHeight()/9, view);

        enemies = new Enemy[2];

        enemies[0] = new Enemy(0, view.getWidth()/2, 6*view.getHeight()/9, view);
        enemies[1] = new Enemy(1, 2*view.getWidth()/2, 6*view.getHeight()/9, view);


        super.width = 9;
        super.height = 50;

        init = new Material[width][height];
        //EMPTY AT TOP FOUR ROWS
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 50; j++) {
                //FOR BRICK
                if(i == 2 && (j == 20 || (j > 23 && j < 27))){
                    init[i][j] = new Material(2, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                //GROW MUSHROOM
                else if(i == 3 && j == 15){
                    init[i][j] = new Material(8, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                //CASTLE CONSTRUCTION
                else if(i == 5 && j == 40){
                    init[i][j] = new Material(4,  j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                else if(i == 6 && (j == 39 || j == 41)){
                    init[i][j] = new Material(5,  j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                else if(i == 6 && j == 40){
                    init[i][j] = new Material(6,  j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                //COIN
                else if(i == 6 && (j > 10 && j < 20)){
                    init[i][j] = new Material(3, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
                }
                else
                    init[i][j] = new Material(0, j * view.getWidth() / 16, i * view.getHeight() / 9, view);
            }
        }

        //FLOOR AT BOTTOM FOUR ROWS
        for(int i = 7; i < 9; i++){
            for(int j = 0; j < 50; j++)
                init[i][j] = new Material(1, j*view.getWidth()/16, i*view.getHeight()/9, view);
        }

    }

    protected void scrollLevel(){
        if(!stopScrolling) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 50; j++) {
                    init[i][j].x -= view.getWidth() / 144;
                }
            }
        }
    }

    public void tick(Canvas c){
        //BACKGROUND
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.CYAN);
        paint.setAntiAlias(true);
        c.drawPaint(paint);

        //SPRITES
        for(int i = 0; i < 9; i++){
            for(int j = 0; j < 50; j++){
                checkpoint(i);
                mario.touched(init[i][j]);
                init[i][j].draw(c);
            }
        }

        //MARIO
        mario.tick(c);

        //ENEMY
        //FIRST ENEMY
        enemies[0].move();
       // enemies[0].touched(mario);
        enemies[0].tick(c);

        //SECOND ENEMY
        if(arrivedActivateEnemy()){
            enemies[1].move();
            enemies[1].tick(c);
        }


        if(mario.dead){
            view.score.resetTime();
            view.score.loseLife();
            if(view.score.life < 0)
                view.score.resetScore();
            view.level = new LevelOne(view);
        }

        //END OF GAME
        if(arrivedCheckpoint())
            view.level = switchTo();

    }

    private void checkpoint(int i){
        if(!stopScrolling) {
            if (mario.x >= init[i][40].x)
                stopScrolling = true;
        }
    }

    private boolean arrivedCheckpoint(){
        return (mario.x >= init[6][40].x);
    }

    private boolean arrivedActivateEnemy(){
        return (mario.x >= init[6][14].x);
    }

    public Level switchTo(){
        mariomusic.stop();
        view.score.addRemainingTimeToScore();
        return new LevelTwo(view);
    }


}
