package jonapostol.supermariobros;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * Created by Jon Apostol on 5/6/2015.
 */
public class SMBThread extends Thread{
    private final SMBView view;
    private static final int FRAME_PERIOD = 5;

    public SMBThread(SMBView view){
        this.view = view;
    }

    public void run(){
        SurfaceHolder sh = view.getHolder();

        while(!Thread.interrupted()) {
            Canvas c = sh.lockCanvas(null);
            try {
                synchronized (sh) {
                    view.tick(c);
                }
            } catch (Exception e) {
            } finally {
                if (c != null)
                    sh.unlockCanvasAndPost(c);

            }
            try {
                Thread.sleep(FRAME_PERIOD);
            } catch(InterruptedException e){
                return;
            }
        }
    }
}
