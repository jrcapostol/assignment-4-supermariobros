package jonapostol.supermariobros;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;

/**
 * Created by Jon Apostol on 5/17/2015.
 */
public class Enemy {
    protected int x;
    protected int y;

    private SMBView view;
    private int alpha = 255;

    private Bitmap image;
    private Rect imageRec;
    private Paint imagePaint;

    protected boolean dead = false;

    public Enemy(int select, int x, int y, SMBView view){
        this.view = view;
        this.x = x;
        this.y = y;

        imagePaint = new Paint();
        imagePaint.setAlpha(alpha);

        switch(select){
            case 0:
                image = BitmapFactory.decodeResource(view.getResources(), R.drawable.goomba);
                break;
            case 1:
                image = BitmapFactory.decodeResource(view.getResources(), R.drawable.bulletbill);
                break;
        }



    }

    public void fireHit(Fireball fireball){
        if (x >= fireball.x + view.getWidth()/16 && x + view.getWidth()/16 <= fireball.x && fireball.y == y)
            dead = true;
    }


    public void move(){
        if(view.level.mario.x + view.getWidth()/16 >= x && view.level.mario.x <= x + view.getWidth()/16){
            if((view.level.mario.y + view.getHeight()/9) >= y && (view.level.mario.y + view.getHeight()/9) <= y + view.getHeight()/81){
                if(!dead) {
                    MediaPlayer deadSXF = MediaPlayer.create(view.getContext(), R.raw.stomp);
                    deadSXF.setLooping(false);
                    deadSXF.start();
                    dead = true;
                    view.score.addScore(10);
                }
            }
            else if (view.level.mario.y == y){
                if(view.level.mario.grown){
                    view.level.mario.grown = false;
                    view.level.mario.growth = 0;
                }
                else
                    view.level.mario.dead = true;
            }
        }

        if(!dead) {
            x -= view.getWidth() / 144;
            if(view.level.mario.x >= view.getWidth()/2){
                x -= view.getWidth()/144;
            }
        }
        else {
            y += view.getHeight() / 63;
            if(view.level.mario.x >= view.getWidth()/2)
                x -= view.getWidth()/72;
        }
    }


    public void tick(Canvas c){
        if(dead){
            alpha -= 51;
            imagePaint.setAlpha(alpha);
        }
        if(y <= view.getHeight()) {
            imageRec = new Rect(x, y, x + view.getWidth() / 16, y + view.getHeight() / 9);
            c.drawBitmap(image, null, imageRec, imagePaint);
        }
    }
}
