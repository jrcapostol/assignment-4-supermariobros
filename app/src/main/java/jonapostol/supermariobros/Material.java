package jonapostol.supermariobros;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Jon Apostol on 5/12/2015.
 */
public class Material {
    protected Bitmap bitmap;
    protected Paint paint;
    protected int type;
    private SMBView view;
    private int alpha = 255;
    private Rect rect;
    protected boolean exists;

    protected int x;
    protected int y;

    protected Material(int type, int x, int y, SMBView view){
        this.view = view;
        this.type = type;
        this.x = x;
        this.y = y;
        paint = new Paint();
        paint.setAlpha(alpha);

        switch(type){
            //EMPTY
            case 0:
                exists = false;
                break;
            //FLOOR
            case 1:
                bitmap = BitmapFactory.decodeResource(view.getResources(), R.drawable.floor);
                exists = true;
                break;
            //BRICK
            case 2:
                bitmap = BitmapFactory.decodeResource(view.getResources(), R.drawable.brick);
                exists = true;
                break;
            //COIN
            case 3:
                exists = false;
                bitmap = BitmapFactory.decodeResource(view.getResources(), R.drawable.coin);
                break;
            //TOP OF CASTLE
            case 4:
                exists = false;
                bitmap = BitmapFactory.decodeResource(view.getResources(), R.drawable.castle);
                break;
            //ENTRANCE OF CASTLE
            case 5:
                exists = false;
                bitmap = BitmapFactory.decodeResource(view.getResources(), R.drawable.castle3);
                break;
            case 6:
                exists = false;
                bitmap = BitmapFactory.decodeResource(view.getResources(), R.drawable.castle1);
                break;
            case 7:
                //LEVEL UP
                exists = true;
                bitmap = BitmapFactory.decodeResource(view.getResources(), R.drawable.mushroom1up);
                break;
            case 8:
                //MUSHROOM GROW
                exists = true;
                bitmap = BitmapFactory.decodeResource(view.getResources(), R.drawable.mushroom1up);
                break;
            case 9:
                //FIREFLOWER
                exists = true;
                bitmap = BitmapFactory.decodeResource(view.getResources(), R.drawable.fireflower);
                break;
        }
        rect = new Rect(x, y, x + view.getWidth()/16, y + view.getWidth()/9);
    }

    public void draw(Canvas c){
        if(type != 0) {
            rect = new Rect(x, y, x + view.getWidth() / 16, y + view.getHeight() / 9);
            c.drawBitmap(bitmap, null, rect, paint);
        }
    }
}
